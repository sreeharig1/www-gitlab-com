---
layout: handbook-page-toc
title: "SA Practices"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

[**SA Practices**](/handbook/customer-success/solutions-architects/sa-practices) - [**Sales Plays**](/handbook/customer-success/solutions-architects/sales-plays) - [**Tools and Resources**](/handbook/customer-success/solutions-architects/tools-and-resources) - [**Career Development**](/handbook/customer-success/solutions-architects/career-development) - [**Demonstration**](/handbook/customer-success/solutions-architects/demonstrations) - [**Processes**](/handbook/customer-success/solutions-architects/processes)

## SA Practices

Solution Architects have various practices:

[Communities of Practice](/handbook/customer-success/solutions-architects/sa-practices/communities-of-practice)

[Deliberate Practice](/handbook/customer-success/solutions-architects/sa-practices/deliberate-practice)

[TODO] Effective Objection Handling
